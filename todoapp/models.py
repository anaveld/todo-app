from django.db import models

class Task(models.Model):
    task= models.TextField()
    is_done = models.BooleanField(default=False)

    def __str__(self):
        return self.task